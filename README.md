#Flutter weather  
Sample flutter project working with openweathermap  
Features:  
- Loading and display about 200k elements (cities and countries)  
- Filtering by name with typed text  
- Navigation to details screen  
- Two-pane layout in landscape  

![picture](https://bitbucket.org/Andrey8000/flutter-weather/downloads/device-2019-04-03-000941.png)

import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:futter_weather/data/city_model.dart';
import 'package:http/http.dart' as http;

class CitiesRepository {

  String _baseUrl = "http://bulk.openweathermap.org/sample/city.list.json.gz";

  void loadData(Function(List<City>) onSuccess, Function(int) onError) async {
    debugPrint("CitiesRepository loadData");
    final response = await http.get(_baseUrl);
    if (response.statusCode != 200) {
      onError(response.statusCode);
      return;
    }
    debugPrint("CitiesRepository loadData OK bytes: ${response.bodyBytes.length}");

    List<City> list = await compute(_parse, response.bodyBytes);
    onSuccess(list);
  }

  static List<City> _parse(bytes) {
    final decoded = GZipCodec().decode(bytes);
    final jsonString = utf8.decode(decoded, allowMalformed: true);
    List<City> list = (json.decode(jsonString) as List)
        .map((data) => new City.fromJson(data))
        .toList();
    return list;
  }
}
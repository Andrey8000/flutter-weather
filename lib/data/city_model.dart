

class City {

  final int id;
  final String name;
  final String country;

  City(this.id, this.name, this.country);

  City._({this.id, this.name, this.country});

  factory City.fromJson(Map<String, dynamic> json) {
    return City._(
      id: json['id'],
      name: json['name'],
      country: json['country'],
    );
  }
}
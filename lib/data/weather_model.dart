

class Weather {

  final WeatherWeather weather;
  final WeatherMain main;

  Weather._({this.weather, this.main});

  factory Weather.fromJson(Map<String, dynamic> jsonMap) {
    return Weather._(
      weather: WeatherWeather.fromJson((jsonMap['weather'] as List<dynamic>)[0]),
      main: WeatherMain.fromJson(jsonMap['main']),
    );
  }
}

class WeatherMain {

  final double temp;
  final double pressure;
  final double humidity;

  WeatherMain._({this.temp, this.pressure, this.humidity});

  factory WeatherMain.fromJson(Map<String, dynamic> json) {
    return WeatherMain._(
      temp: json['temp'].toDouble(),
      pressure: json['pressure'].toDouble(),
      humidity: json['humidity'].toDouble(),
    );
  }
}

class WeatherWeather {

  final double id;
  final String main;
  final String description;
  final String icon;

  WeatherWeather._({this.id, this.main, this.description, this.icon});

  factory WeatherWeather.fromJson(Map<String, dynamic> json) {
    return WeatherWeather._(
      id: json['id'].toDouble(),
      main: json['main'],
      description: json['description'],
      icon: json['icon'],
    );
  }
}
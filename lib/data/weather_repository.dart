import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:futter_weather/data/weather_model.dart';
import 'package:http/http.dart' as http;

class WeatherRepository {

  String _domain = "api.openweathermap.org";
  String _requestUrl = "/data/2.5/weather";

  void loadData(int id, String lang, String apiKey, Function(Weather) onSuccess, Function(int) onError) async {
    debugPrint("WeatherRepository loadData");
    var queryParameters = {
      'id': '$id',
      'lang': '$lang',
      'units': 'metric',
      'APPID': '$apiKey',
    };
    Uri uri = Uri.http(_domain, _requestUrl, queryParameters);
    final response = await http.get(uri);
    if (response.statusCode != 200) {
      onError(response.statusCode);
      return;
    }
    debugPrint("WeatherRepository loadData OK: ${response.body}");

    Weather result = Weather.fromJson(json.decode(response.body));
    onSuccess(result);
  }
}
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:futter_weather/presentation/view_caches.dart';
import 'package:futter_weather/data/cities_repository.dart';
import 'package:futter_weather/data/weather_repository.dart';
import 'package:futter_weather/presentation/screens_mediator.dart';

class DepsContainer {

  Injector init(Injector injector) {

    injector.map<ScreensMediator>((injector) => ScreensMediator(), isSingleton: true);
    injector.map<CitiesRepository>((injector) => CitiesRepository(), isSingleton: true);
    injector.map<WeatherRepository>((injector) => WeatherRepository(), isSingleton: true);
    injector.map<ViewCaches>((injector) => ViewCaches(), isSingleton: true);

    return injector;
  }
}

Injector injector() => Injector.getInjector();
import 'package:flutter/material.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';
import 'package:futter_weather/di/deps_container.dart';
import 'package:futter_weather/presentation/cities_list/cities_list_screen.dart';

void main() {
  DepsContainer().init(Injector.getInjector());
  runApp(CitiesListScreen());
}

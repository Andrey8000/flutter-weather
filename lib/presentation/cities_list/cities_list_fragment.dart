
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:futter_weather/presentation/view_caches.dart';
import 'package:futter_weather/data/cities_repository.dart';
import 'package:futter_weather/data/city_model.dart';
import 'package:futter_weather/di/deps_container.dart';
import 'package:futter_weather/presentation/screens_mediator.dart';

class CitiesListFragment extends StatefulWidget {

  @override
  State<CitiesListFragment> createState() {
    return _CitiesListState();
  }
}

class _CitiesListState extends State<CitiesListFragment> {

  final CitiesRepository _repo = injector().get<CitiesRepository>();
  final ScreensMediator _mediator = injector().get<ScreensMediator>();
  final ViewCaches _caches = injector().get<ViewCaches>();
  final TextEditingController _textController = TextEditingController();
  _UiModel _uiModel = _UiModel(List(), false);

  _CitiesListState() {
    debugPrint("_CitiesListState");

    if (_caches.cities.isNotEmpty) {
      _updateView(_UiModel(_caches.cities, true));
    } else {
      _repo.loadData((cities) =>
      {
      _saveToCacheAndUpdateView(_UiModel(cities, true))
      }, (errorCode) =>
      {
      _updateView(_UiModel(List(), true))
      });
    }
  }

  void _saveToCacheAndUpdateView(_UiModel uiModel) {
    _caches.cities = uiModel.cities;
    _updateView(uiModel);
  }

  @override
  void initState() {
    _textController.addListener(() {
      if (_uiModel.loadingFinished) {
        _updateView(_UiModel(
            _caches.cities.where((city) =>
                city.name.toLowerCase().contains(_textController.text.toLowerCase())).toList(),
            true)
        );
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  void _updateView(_UiModel uiModel) {
    if (mounted) {
      setState(() {
        _uiModel = uiModel;
      });
    } else {
      _uiModel = uiModel;
    }
  }

  @override
  Widget build(BuildContext context) {
    return _createCitiesListMainWidget(_uiModel, (city) => { _mediator.selectedCity.setValue(city) }, _textController);
  }
}

Column _createCitiesListMainWidget(_UiModel model, Function(City) onTap, TextEditingController searchController) {
  return Column(
    children: <Widget>[
      SizedBox(
          width: double.infinity,
          child: Center(
              child: Container(
                child: TextField(
                  controller: searchController,
                  maxLines: 1,
                  decoration: InputDecoration(
                      border: UnderlineInputBorder(),
                      hintText: 'Search (total elements: ${model.cities.length})'
                  ),
                ),
                padding: EdgeInsets.all(8.0),
              ))),
      _createMainView(model, onTap),
    ],
  );
}

Widget _createMainView(_UiModel model, Function(City) onTap) {
  if (model.loadingFinished) {
    return _createItemsList(model.cities, onTap);
  } else {
    return _createLoadingView();
  }
}

Widget _createLoadingView() {
  return Container(
    child: CircularProgressIndicator(),
    padding: EdgeInsets.all(16.0),
  );
}

Widget _createItemsList(List<City> cities, Function(City) onTap) {
  return Expanded(
      child: Container(
          child: ListView.builder(
            itemCount: cities.length,
            padding: EdgeInsets.all(1.0),
            itemBuilder: (context, position) {
              return _listItem(context, position, cities[position], onTap);
            },
          )));
}

Widget _listItem(
    BuildContext context, int position, City city, Function(City) onTap) {
  return Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[InkWell(
        child: ListTile(
          contentPadding: EdgeInsets.all(10.0),
          title: Text("${city.name}, ${city.country}"),
        ),
        onTap: () => onTap(city),
      )]);
}


class _UiModel {
  final List<City> cities;
  final bool loadingFinished;

  _UiModel(this.cities, this.loadingFinished);
}
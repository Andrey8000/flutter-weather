import 'package:flutter/material.dart';
import 'package:futter_weather/data/city_model.dart';
import 'package:futter_weather/di/deps_container.dart';
import 'package:futter_weather/presentation/cities_list/cities_list_fragment.dart';
import 'package:futter_weather/presentation/city_weather/city_fragment.dart';
import 'package:futter_weather/presentation/city_weather/city_screen.dart';
import 'package:futter_weather/presentation/screens_mediator.dart';
import 'package:futter_weather/utils/utils.dart';

class CitiesListScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Weather',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: CitiesListPage(title: 'Weather'),
    );
  }
}

class CitiesListPage extends StatefulWidget {
  final String title;

  CitiesListPage({Key key, this.title}) : super(key: key);

  @override
  _CitiesListPageState createState() => _CitiesListPageState();
}

class _CitiesListPageState extends State<CitiesListPage> {
  final ScreensMediator _mediator = injector().get<ScreensMediator>();
  Function() observeCityDisposable;
  CitiesListFragment _listFragment;

  _CitiesListPageState() {
    debugPrint("_CitiesListPageState");
    observeCityDisposable = _mediator.selectedCity.observe((city) => {
      _openDetailsScreenIfPortrait(city)
    });
  }

  @override
  void initState() {
    _listFragment = CitiesListFragment();
    super.initState();
  }

  @override
  void dispose() {
    observeCityDisposable();
    super.dispose();
  }

  void _openDetailsScreenIfPortrait(City city) {
    if (context == null) {
      return;
    }
    if (isPortrait(context)) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => CityScreen(city)),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: OrientationBuilder(
        builder: (context, orientation) {
          return isPortrait(context) ? _listFragment : createDoubleWidget();
        },
      ),
    );
  }

  Row createDoubleWidget() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
            child: Card(
              margin: EdgeInsets.fromLTRB(8, 8, 0, 8),
                child: _listFragment
            )
        ),
        Expanded(
            child: Card(
                margin: EdgeInsets.all(8),
                child: CityFragment()
            )
        )
      ],
    );
  }
}

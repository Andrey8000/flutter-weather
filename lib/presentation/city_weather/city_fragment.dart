
import 'package:flutter/cupertino.dart';
import 'package:futter_weather/data/api_key.dart';
import 'package:futter_weather/data/city_model.dart';
import 'package:futter_weather/data/weather_repository.dart';
import 'package:futter_weather/di/deps_container.dart';
import 'package:futter_weather/presentation/screens_mediator.dart';

class CityFragment extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _CityFragmentState();
  }
}

class _CityFragmentState extends State<CityFragment> {

  final WeatherRepository _repo = injector().get<WeatherRepository>();
  final ScreensMediator _mediator = injector().get<ScreensMediator>();
  _UiModel _uiModel = _UiModel("", "");
  Function() observeCityDisposable;

  _CityFragmentState() {
    debugPrint("_CityFragmentState");

    observeCityDisposable = _mediator.selectedCity.observe((city) => {
      loadCityWeather(city)
    });
  }

  @override
  void dispose() {
    observeCityDisposable();
    super.dispose();
  }

  void loadCityWeather(City city) {
    if (city == null) {
      _updateView(_UiModel("", ""));
      return;
    }
    _repo.loadData(city.id, "ru", apiKey, (weather) => {
    _updateView(_UiModel("${city.name}: ${weather.main.temp}, ${weather.weather.description}",
        "http://openweathermap.org/img/w/${weather.weather.icon}.png"))
    }, (error) => {
    _updateView(_UiModel("Error: $error", ""))
    });
  }

  void _updateView(_UiModel uiModel) {
    if (mounted) {
      setState(() {
        _uiModel = uiModel;
      });
    } else {
      _uiModel = uiModel;
    }
  }

  @override
  Widget build(BuildContext context) {
    return _createCityDetailsWidget(_uiModel);
  }
}

Widget _createCityDetailsWidget(_UiModel model) {
  if (model == null) {
    return Column();
  }
  return
    Center(
        child: Column(
            children: <Widget>[
              Center(
                child: Text(model.mainText),
              ),
              Image.network(
                model.iconUrl,
                fit: BoxFit.cover,
                height: 60.0,
                width: 60.0,
              )
            ],
            mainAxisSize: MainAxisSize.min
        ));
}

class _UiModel {
  final String mainText;
  final String iconUrl;

  _UiModel(this.mainText, this.iconUrl);
}
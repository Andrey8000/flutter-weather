import 'package:flutter/material.dart';
import 'package:futter_weather/data/city_model.dart';
import 'package:futter_weather/presentation/city_weather/city_fragment.dart';

class CityScreen extends StatefulWidget {
  final City _city;

  CityScreen(this._city);

  @override
  State<StatefulWidget> createState() => _CityScreenState(_city);
}

class _CityScreenState extends State<CityScreen> {

  City _city;

  _CityScreenState(City city) {
    _city = city;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("City: ${_city.name}, ${_city.country}"),
      ),
      body: Center(
        child: CityFragment(),
      ),
    );
  }
}
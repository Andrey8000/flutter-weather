import 'package:futter_weather/data/city_model.dart';
import 'package:futter_weather/utils/observable.dart';

class ScreensMediator {

  final ObservableValue<City> selectedCity = ObservableValue();
}
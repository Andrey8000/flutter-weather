
class ObservableValue<T> {
  T _lastValue;

  List<Function(T)> _observers = List();

  void setValue(T value) {
    _lastValue = value;
    _observers.forEach((listener) => listener(value));
  }

  Function() observe(Function(T) observer) {
    observer(_lastValue);
    _observers.add(observer);

    return () => {_observers.remove(observer)};
  }
}

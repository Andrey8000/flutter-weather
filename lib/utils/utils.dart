
import 'package:flutter/cupertino.dart';

bool isPortrait(BuildContext context) => MediaQuery.of(context).orientation == Orientation.portrait;